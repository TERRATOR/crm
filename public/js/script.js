jQuery(document).ready(function ($) {
    $("#show-bug-form").click(function () {
        //show modal layer
        $("#bug_form").addClass('is-visible');
    });

    //close modal
    $('.cd-user-modal').on('click', function (event) {
        if ($(event.target).is($("#create_task_form")) || $(event.target).is('.cd-close-form')) {
            $("#create_task_form").removeClass('is-visible');
        }
    });

    //close modal when clicking the esc keyboard button
    $(document).keyup(function (event) {
        if (event.which == '27') {
            $("#create_task_form").removeClass('is-visible');
        }
    });

});

var app = new Vue({
    el: "#application",
    delimiters: ['${', '}'],
    data: {
        tasks: [],
        users: [],
        abstract_days: [],
        onLoad: false,
        task_types:[],
        new_task_type:0,
        resources:[],
        new_task_resource:0,
        new_task_executor:0,
        new_task_time:"",
        new_task_status:0,
        statuses:[],
        new_task_name:""
    },
    mounted: function () {
        var self = this;
        $.ajax({
            url: "/",
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                self.onLoad = true;
            },
            success: function (json) {
                self.onLoad = false;
                if (json.code != 200) {
                    toastr['error'](json.status);
                    return false;
                }
                self.abstract_days = json.data.days;
                self.task_types = json.data.task_types;
                self.users = json.data.users;
                self.tasks = json.data.tasks;
                self.resources = json.data.resources;
                self.statuses = json.data.statuses;
            },
            error: function (data) {
                console.log(data);
                self.onLoad = false;
            }
        });
        return false;
    },
    methods: {
        openCreateTaskForm:function() {
            //show modal layer
            $("#create_task_form").addClass('is-visible');
        },
        validateEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },
        loadTaskById: function (id) {
            var self = this;
            $.ajax({
                url: "/",
                type: "GET",
                dataType: "json",
                data: {
                    id: id
                },
                beforeSend: function () {
                    self.onLoad = true;
                },
                success: function (json) {
                    self.onLoad = false;
                    if (json.code !== 200) {
                        toastr['error'](json.status);
                    }
                },
                error: function (data) {
                    console.log(data);
                    self.onLoad = false;
                }
            });
        },
        createNewTask:function(e) {
            e.preventDefault();
            var self = this;
            $.ajax({
                url: "/crm/create-task",
                type: "POST",
                dataType: "json",
                data: {
                    name: self.new_task_name
                },
                beforeSend: function () {
                    self.onLoad = true;
                },
                success: function (json) {
                    self.onLoad = false;
                    if (json.code !== 200) {
                        toastr['error'](json.status);
                    }
                },
                error: function (data) {
                    console.log(data);
                    self.onLoad = false;
                }
            });
        }
    }
});