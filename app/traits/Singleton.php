<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Traits;

trait Singleton {
    
    /**
     * Requests data from API
     * @param type $url
     * @return array|string (JSON)
     */
    public function recieveDataFromAPI(string $url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Accept: application/json']);
        $result = curl_exec($ch);
        $statuscode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $statustext = curl_getinfo($ch);
        curl_close($ch);
        if ($statuscode !== 200) {
            return ['status' => $statustext];
        }
        return $this->response->setJsonContent($result);
    }
    
    /**
     * Validates input data (\Phalcon\Http\Request) based on Model rules const FILTER_METHODS
     * @param type $input_data
     * @param type $model
     * @return type
     */
    public function sanitizeData($input_data, $model) {
        if(!defined(str_replace("\/", "\\", $model)."::FILTER_METHODS")) {
            throw new \Exception('Model does not have const FILTER_METHODS ');
        }
        $filter = new \Phalcon\Filter();
        $filtered_data = [];
        foreach ($input_data as $key => &$value) {
            // check if REQUEST key exists
            if(array_key_exists($key, $model::FILTER_METHODS)) {
                // check if validate rule in model exists
                if(!empty($model::FILTER_METHODS[$key])) {
                    $filtered_data[$key] = $filter->sanitize($value, $model::FILTER_METHODS[$key]);
                } else {
                    //throw new \Exception('Поле '.$key.' отсутствует в модели фильтров.');
                }
            }
        }
        return $filtered_data;
    }
    
    /**
     * Validates assigned model data 
     * @param type $model
     * @return boolean
     */
    public function validateModel($model) {
        $model->validation();
        $validation_message = $model->getMessages();
        if(count($validation_message) > 0) {
            $errors = "";
            foreach ($validation_message as $message) {
                $errors .= $message .'</br>';
            }
            $errors = trim($errors, '</br>');
            return $errors;
        }
        return true;
    }
    
    /**
     * Validates input data
     * @param \Phalcon\Validation $validation
     * @param array $input_array
     * @throws \Exception
     */
    public function validateInputData(\Phalcon\Validation $validation, $input_array) {
        $validation_messages = $validation->validate($input_array);
        if (count($validation_messages)) {
            $errors = "";
            foreach ($validation_messages as $message) {
                $errors .= $message . "<br/>";
            }
            throw new \Exception($errors. implode('::', $input_array));
        }
    }
}