<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Criteria;

use Phalcon\Mvc\Model\Criteria as Criteria;

class TaskCriteria extends Criteria {
    
    public function getActiveRecords($status) {
        return $this->andWhere('deleted = 0 AND status = :status:')->bind(['status' => $status]);
    }
    
}