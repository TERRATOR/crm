<?php

$router = $di->getRouter(false);
$router->setUriSource(Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

// Define your routes here
$router->add("/", ['namespace' => 'App\Controllers\\',
            "controller" => "index", 
            'action' => 'index']);

$router->add("/get-data-of-task", ['namespace' => 'App\Controllers\\',
            "controller" => "index", 
            'action' => 'getDataForTask']);

$router->add("/create-task", ['namespace' => 'App\Controllers\\',
            "controller" => "index", 
            'action' => 'createTask']);

$router->add("/update-task", ['namespace' => 'App\Controllers\\',
            "controller" => "index", 
            'action' => 'updateTask']);

$router->add("/delegate-task", ['namespace' => 'App\Controllers\\',
            "controller" => "index", 
            'action' => 'delegateTask']);

$router->add("/change-status", ['namespace' => 'App\Controllers\\',
            "controller" => "index", 
            'action' => 'changeStatus']);

$router->handle();
