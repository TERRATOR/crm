<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
/*$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
)->register(); */

$loader->registerNamespaces(array(
    'App\Domains\TaskHandler' => $config->application->handlers,
    'App\Controllers' => $config->application->controllersDir,
    'App\Models' => $config->application->modelsDir,
    'App\Migrations' => $config->application->migrationsDir,
    'App\Traits' => $config->application->traitsDir,
    'App\Criteria' => $config->application->criteriaDir,
    'App\Validations' => $config->application->validationDir
))->register();
