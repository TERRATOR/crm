<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as Flash;
use Phalcon\Cache\Frontend\Data as FrontData;
use Phalcon\Cache\Backend\Libmemcached as Memcached;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class,
        '.volt' => "Phalcon\Mvc\View\Engine\Volt"
    ]);

    return $view;
});

/**
         * Setting up volt
         */
        $di->set('Phalcon\Mvc\View\Engine\Volt', function ($view, $di) {
            $volt = new VoltEngine($view, $di);
            $volt->setOptions(array(
                "compiledPath" => $_SERVER['DOCUMENT_ROOT']."/cache/volt/"
            ));
            $compiler = $volt->getCompiler();
            $compiler->addFunction('print_r', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "print_r($resolvedArgs)";
            });
            $compiler->addFunction('ceil', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "ceil($resolvedArgs)";
            });
            $compiler->addFunction('round', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "round($resolvedArgs)";
            });
            $compiler->addFunction('strtotime', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "strtotime($resolvedArgs)";
            });
            $compiler->addFunction('floor', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "floor($resolvedArgs)";
            });
            $compiler->addFunction('date', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "date($resolvedArgs)";
            });
            $compiler->addFunction('explode', function($resolvedArgs, $exprArgs) use($di, $compiler) {
                return "explode($resolvedArgs)";
            });
            $compiler->addFunction('in_array', 'in_array');
            $compiler->addFunction('intval', 'intval');
            $compiler->addFunction('number_format', 'number_format');
            $compiler->addFunction('substr_count', 'substr_count');
            return $volt;
        }, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

// кеширование для быстрых динамических данных
$di->set('cache', function () {
    $frontCache = new FrontData(["lifetime" => 120]);

    return new Memcached($frontCache, [
        "servers" => [["host" => "127.0.0.1", "port" => "11211"]]]
    );
});
