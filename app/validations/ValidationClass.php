<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validations;

use Phalcon\Validation;

class ValidationClass extends \App\Validations\ValidationCase {
    
    private $rules;
    private $errors;
    private $validationObject;
    
    public function __construct($rules) {
        $this->validationObject = new Validation();
        // override rules
        if(count($rules) > 0) {
            $this->rules = $rules;
        } else {
            // some defualt rules
            $this->rules = [['id', 'num', 'message' => 'Incorrect number of identity']];
        }
    }
    
    protected function mapValidators() {
        return ['num' => "\Phalcon\Validation\Validator\Numericality", 
                'empty' => "\Phalcon\Validation\Validator\PresenceOf",
                'strlen' => "\Phalcon\Validation\Validator\StringLength",
                "email" => "\Phalcon\Validation\Validator\Email",
                'ip' => "\App\Validations\IpValidator"];
    }
    
    /**
     * 
     * @param type $array
     * @return type
     */
    public function validate($array) {
        foreach($array as $key => &$value) {
            $current_rule = $this->returnRecordFromRules($key);
            if($current_rule != false) {
                $this->createRuleForCurrentField($current_rule);
            }
        }
        return $this->validationObject->validate($array);
    }
    
    /**
     * Creates new validation rule based on $this->rules array
     * @param type $current_field
     */
    protected function createRuleForCurrentField($current_field) {
        $parameters = $this->rebuildArray($current_field);
        $get_validator = $this->mapValidators();
        if(is_array($current_field[1])) {
            $inner_rules = $current_field[1];
            foreach($inner_rules as &$inner_rule) {
                // same as $validation->add("name", new \Phalcon\Validation\Validator\PresenceOf(["message" => "Поле Название пустое"]));
                $this->validationObject->add($current_field[0], new $get_validator[$inner_rule]($parameters));
            }
        } else {
            $this->validationObject->add($current_field[0], new $get_validator[$current_field[1]]($parameters));
        }
    }

    /**
     * returns record from array using input key given from POST or GET input array
     * @param string $value
     * @return mixed array|boolean
     */
    protected function returnRecordFromRules($value) {
        foreach($this->rules as $rule) {
            if($rule[0] === $value) {
                return $rule;
            }
        }
        return false;
    }

    /**
     * Removes 0 and 1 indexes from array and rebuild keys
     * @param type $array
     * @return type
     */
    protected function rebuildArray($array) {
        unset($array[0]);
        unset($array[1]);
        return array_values($array);
    }
    
    //class_exists('\\Foo\\Bar'))
}