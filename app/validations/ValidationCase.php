<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validations;

abstract class ValidationCase extends \Phalcon\Mvc\User\Component{
    
    abstract protected function mapValidators();
    
    abstract public function validate($array);
}