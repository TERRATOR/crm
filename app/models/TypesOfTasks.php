<?php

namespace App\Models;

use Phalcon\Mvc\Model\Behavior\SoftDelete;

class TypesOfTasks extends \Phalcon\Mvc\Model {
    
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=10, nullable=false)
     */

    public $id;
    
    /**
     *
     * @var integer
     * @Column(column="company_id", type="integer", length=10, nullable=false)
     */
    public $company_id;

    /**
     *
     * @var integer
     * @Column(column="updated_at", type="integer", length=10, nullable=false)
     */
    public $updated_at;

    /**
     *
     * @var integer
     * @Column(column="hc_id", type="integer", length=10, nullable=true)
     */
    public $hc_id;

    /**
     *
     * @var string
     * @Column(column="title", type="string", length=60, nullable=false)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(column="description", type="string", length=255, nullable=true)
     */
    public $description;

    /**
     *
     * @var integer
     * @Column(column="status", type="integer", length=3, nullable=false)
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->addBehavior(
                    new SoftDelete(["field" => "status", "value" => self::STATUS_DEFAULT]
                )
        );
        $this->setSchema("test");
        $this->setSource("types_of_tasks");
        
        $this->belongsTo('company_id', "Tasks", 'id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'types_of_tasks';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TypesOfTasks[]|TypesOfTasks|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TypesOfTasks|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }
}
