<?php

namespace App\Models;

class Cards extends \Phalcon\Mvc\Model
{
    
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(column="company_id", type="integer", length=10, nullable=false)
     */
    public $company_id;

    /**
     *
     * @var string
     * @Column(column="image", type="string", length=512, nullable=false)
     */
    public $image;

    /**
     *
     * @var integer
     * @Column(column="status", type="integer", length=4, nullable=false)
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->addBehavior(
                    new SoftDelete(["field" => "status", "value" => self::STATUS_DEFAULT]
                )
        );
        $this->setSchema("test");
        $this->setSource("cards");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cards';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards[]|Cards|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
