<?php

namespace App\Models;

use Phalcon\Mvc\Model\Behavior\{
    Timestampable,
    SoftDelete
};
use Phalcon\Validation;
use Phalcon\Validation\Validator\{
    Uniqueness,
    PresenceOf,
    StringLength,
    Between,
    Numericality
};

class Tasks extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=64, nullable=false)
     */
    public $name;

    /**
     *
     * @var integer
     * @Column(column="company_id", type="integer", length=10, nullable=false)
     */
    public $company_id;

    /**
     *
     * @var integer
     * @Column(column="author_id", type="integer", length=10, nullable=false)
     */
    public $author_id;

    /**
     *
     * @var integer
     * @Column(column="executor_id", type="integer", length=10, nullable=false)
     */
    public $executor_id;

    /**
     *
     * @var integer
     * @Column(column="clone_parent_id", type="integer", length=10, nullable=true)
     */
    public $clone_parent_id;

    /**
     *
     * @var integer
     * @Column(column="clone_child_id", type="integer", length=10, nullable=true)
     */
    public $clone_child_id;

    /**
     *
     * @var integer
     * @Column(column="handler", type="integer", length=3, nullable=false)
     */
    public $handler;

    /**
     *
     * @var integer
     * @Column(column="type", type="integer", length=10, nullable=false)
     */
    public $type;

    /**
     *
     * @var integer
     * @Column(column="resource", type="integer", length=5, nullable=false)
     */
    public $resource;

    /**
     *
     * @var integer
     * @Column(column="resource_id", type="integer", length=10, nullable=false)
     */
    public $resource_id;

    /**
     *
     * @var integer
     * @Column(column="sub_resource", type="integer", length=5, nullable=true)
     */
    public $sub_resource;

    /**
     *
     * @var integer
     * @Column(column="sub_resource_id", type="integer", length=10, nullable=true)
     */
    public $sub_resource_id;

    /**
     *
     * @var integer
     * @Column(column="status", type="integer", length=3, nullable=false)
     */
    public $status;

    /**
     *
     * @var integer
     * @Column(column="status_at", type="integer", length=10, nullable=false)
     */
    public $status_at;

    /**
     *
     * @var string
     * @Column(column="description", type="string", length=1000, nullable=false)
     */
    public $description;

    /**
     *
     * @var string
     * @Column(column="comment", type="string", length=1000, nullable=true)
     */
    public $comment;

    /**
     *
     * @var integer
     * @Column(column="deadline_at", type="integer", length=10, nullable=false)
     */
    public $deadline_at;

    /**
     *
     * @var integer
     * @Column(column="created_at", type="integer", length=10, nullable=false)
     */
    public $created_at;

    /**
     *
     * @var integer
     * @Column(column="updated_at", type="integer", length=10, nullable=false)
     */
    public $updated_at;

    /**
     *
     * @var integer
     * @Column(column="updated_at", type="integer", length=10, nullable=true)
     */
    public $deleted;

    const STATUS_DEFAULT = 1;         // Для задач без статуса
    const STATUS_NEW = 2;             // Новая
    const STATUS_CLOSE = 3;           // Закрыта/выполненна
    const STATUS_CLONE_CLOSE = 4;     // Пернесенна
    const STATUS_CANCEL = 5;          // Отклоненна

    /**
     * Initialize method for model.
     */

    public function initialize() {
        $this->setSchema("test");
        $this->setSource("tasks");
        $this->addBehavior(new Timestampable(['beforeCreate' => ['field' => 'updated_at'],
            'beforeUpdate' => ['field' => 'updated_at']]));
        //SoftDelete api Phalcon
        $this->addBehavior(
                new SoftDelete(
                ['field' => 'deleted',
            'value' => time()]
                )
        );
        $this->hasOne('id', 'TypesOfTasks', 'company_id', ['alias' => 'TypesOfTasks']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'tasks';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tasks[]|Tasks|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tasks|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    public function getCreatedAt() {
        return $this->created_at = date('d-m-Y H:i:s', $this->created_at);
    }

    /**
     * Returns const resources
     * @return Array
     */
    public static function returnResourceTypes() {
        return [[1 => ['name' => 'Объект недвижимости', 'UrlAPI' => 'https://www.api.com/getData']],
            [2 => ['name' => 'Заявка', 'UrlAPI' => 'https://www.api.com/getData']],
            [3 => ['name' => 'Лид', 'UrlAPI' => 'https://www.api.com/getData']],
            [4 => ['name' => 'Пользователь', 'UrlAPI' => 'https://www.api.com/getData']],
            [5 => ['name' => 'Мероприятие', 'UrlAPI' => 'https://www.api.com/getData']]];
    }

    /**
     * Returns class resource using identity from db
     * @param integer $id
     * @return Array|boolen
     */
    public static function returnResource($id) {
        $resources = self::returnResourceTypes();
        foreach ($resources as $key => $value) {
            if ($key == $id) {
                return $value;
            }
        }
        return false;
    }

    /**
     * Returns const handlers from /app/domain/ instead abstract class TaskHandler. 
     * It should be write down manually by programmer
     * @return Array
     */
    public static function returnHandlers() {
        return [['id' => 1, 'class' => '\App\Domains\TaskHandler\TaskHandlerManualDefault'],
            ['id' => 2, 'class' => '\App\Domains\TaskHandler\TaskHandlerObjOperationStatus'],
            ['id' => 3, 'class' => '\App\Domains\TaskHandler\TaskHandlerAdvertising'],
            ['id' => 4, 'class' => '\App\Domains\TaskHandler\TaskHandlerHoliday']];
    }

    /**
     * Returns class handler using identity from db
     * @param integer $id
     * @return string|boolen
     */
    public static function returnHandlerClass($id) {
        $handlers = self::returnHandlers();
        foreach ($handlers as $handler) {
            if ($handler['id'] == $id) {
                return $handler['class'];
            }
        }
        return false;
    }

    public static function returnStatuses() {
        return [['id' => self::STATUS_DEFAULT, 'name' => 'Без статуса'],
            ['id' => self::STATUS_NEW, 'name' => 'Новая'],
            ['id' => self::STATUS_CLOSE, 'name' => 'Закрыта/выполненна'],
            ['id' => self::STATUS_CLONE_CLOSE, 'name' => 'Пернесенна'],
            ['id' => self::STATUS_CANCEL, 'name' => 'Отклоненна']];
    }

    const FILTER_METHODS = ["id" => 'int', 'name' => 'string', 'company_id' => 'int',
        'author_id' => 'int', 'executor_id' => 'int', 'clone_parent_id' => 'int',
        'clone_child_id' => 'int', 'handler' => 'int', 'type' => 'int',
        'resource' => 'int', 'resource_id' => 'int', 'sub_resource' => 'int',
        'sub_resource_id' => 'int', 'status' => 'int', 'status_at' => 'int',
        'description' => 'string', 'comment' => 'string', 'deadline_at' => 'int'];

    public function validation() {
        $validator = new Validation();

        $validator->add(
                'description', new StringLength([
            'max' => 1000,
            'min' => 1,
            'messageMaximum' => 'Описание задачи должно быть до 1000 символов.',
            'messageMinimum' => 'Описание задачи должно быть от 1 символа.'
                ])
        );

        $validator->add(
                'name', new StringLength([
            'max' => 64,
            'min' => 1,
            'messageMaximum' => 'Название задачи должно быть до 64 символов.',
            'messageMinimum' => 'Название задачи должно быть от 1 символа.'
                ])
        );

        $validator->add(
                'company_id', new Numericality([
            'message' => 'Отсутствует компания.'
                ])
        );

        $validator->add(
                'author_id', new Numericality([
            'message' => 'Отсутствует автор задачи.'
                ])
        );

        $validator->add(
                'handler', new Numericality([
            'message' => 'Отсутствует обработчик задачи.'
                ])
        );

        $validator->add(
                'type', new Numericality([
            'message' => 'Отсутствует тип задачи.'
                ])
        );

        $validator->add(
                'resource', new Numericality([
            'message' => 'Отсутствует ресурс задачи.'
                ])
        );

        $validator->add(
                'resource_id', new Numericality([
            'message' => 'Отсутствует идентификатор ресурса задачи.'
                ])
        );

        $validator->add(
                'resource_id', new Numericality([
            'message' => 'Отсутствует идентификатор ресурса задачи.'
                ])
        );

        $validator->add(
                'status', new Numericality([
            'message' => 'Отсутствует статус задачи.'
                ])
        );

        return $this->validate($validator);
    }

    public function shiftValidation() {
        $validator = new Validation();

        $validator->add(
                'comment', new StringLength([
            'max' => 1000,
            'min' => 1,
            'messageMaximum' => 'Комментарий задачи должен быть до 1000 символов.',
            'messageMinimum' => 'Комментарий задачи должен быть от 1 символа.'
                ])
        );

        $validator->add(
                'deadline_at', new PresenceOf([
            'message' => 'Отсутствует время крайнего срока задачи.'
                ])
        );

        $validator->add(
                'id', new Numericality([
            'message' => 'Отсутствует идентификатор задачи.'
                ])
        );

        return $this->validate($validator);
    }

    public function beforeValidationOnCreate() {
        if (empty($this->executor_id)) {
            $this->executor_id = $this->author_id;
        }
        if (empty($this->status_at)) {
            $this->status_at = time();
        }
        $this->created_at = time();
        $this->updated_at = time();
        $this->status_at = time();
    }

    public function beforeValidationOnUpdate() {
        $this->updated_at = time();
    }

    /**
     * @return PostCriteria
     */
    public static function query(\Phalcon\DiInterface $dependencyInjector = null) {
        if (!is_object($dependencyInjector)) {
            $dependencyInjector = \Phalcon\Di::getDefault();
        }
        
        $criteria = new \App\Criteria\TaskCriteria();
        $criteria->setDI($dependencyInjector);
        $criteria->setModelName(get_called_class());
        return $criteria;
    }

}
