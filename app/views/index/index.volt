<div class="page-header">
    <h1>Congratulations!</h1>
</div>

<p>You're now flying with Phalcon. Great things are about to happen!</p>

<p>This page is located at <code>views/index/index.phtml</code></p>
<div class="flex" id="application">
    {% for task in tasks %}
        id = {{task.id}} name = {{task.name}} type = {{task.type}}<br>
    {% endfor %}
    <div class="col-xs-12">
        <a href="javascript: void(0);" v-on:click="openCreateTaskForm" class="btn btn-success">
            <i class="fa fa-plus"></i> Add Task</a>
    </div>
    <div v-for="(item, index) in abstract_days" v-bind:index="index" v-bind:key="index" class="day-block">
        <p>${item.day}</p>
        <p>${(index+1)}</p>
        <p>${item.month}</p>
    </div>
    
    <!-- Bug modal window -->
    <div class="cd-user-modal" id='create_task_form'> <!-- this is the entire modal form, including the background -->
        <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
            <ul class="cd-switcher">
                <li><a>Создание задачи</a></li>
            </ul>

            <div id="cd-author-adv-form"> <!-- callback_advertisemnet_author_form -->
                <form class="cd-form">
                    <p class="fieldset">
                        <label class="image-replace" for="task-name">Название</label>
                        <input class="full-width has-border inquiry-input" id="task-name" placeholder="Название" v-model="new_task_name">
                    </p>
                    <p class="fieldset">
                        <label class="image-replace" for="task-type">Тип задачи</label>
                        <select v-model="new_task_type" id="task-type">
                            <option value="0">Выберите тип задачи</option>
                            <option v-for="(task, tindex) in task_types" v-bind:value="task.id">${task.title}</option>
                        </select>
                        <select v-model="new_task_resource">
                            <option value="0">Ресурс задачи</option>
                            <option v-for="(resource, rindex) in resources" v-bind:value="resource.id">${resource.name}</option>
                        </select>
                        <select v-model="new_task_executor">
                            <option value="0">Исполнитель</option>
                            <option v-for="(executor, eindex) in users" v-bind:value="executor.id">${executor.name} - ${executor.email}</option>
                        </select>
                        <select v-model="new_task_status">
                            <option v-for="(status, sindex) in statuses" v-bind:value="status.id">${status.name}</option>
                        </select>
                    </p> 
                    <p class="fieldset">
                        <label class="image-replace" for="task-description">Описание</label>
                        <textarea class="full-width has-border inquiry-input" id="task-description" placeholder="Описание"></textarea>
                    </p>
                    <p class="fieldset">
                        <label class="image-replace" for="task-type">Срок окончания задачи</label>
                        <input type="datetime-local" v-model="new_task_time">
                    </p>
                    <p class="fieldset">
                        <input class="full-width" type="submit" value="Создать" v-on:click="createNewTask">
                    </p>
                </form>

                <!-- <a href="#0" class="cd-close-form">Close</a> -->
            </div> <!-- callback_advertisemnet_author_form -->

            <a href="#0" class="cd-close-form">Закрыть</a>
        </div> <!-- cd-user-modal-container -->
    </div> <!-- cd-user-modal -->
    <!-- END Bug modal window -->
</div>