<?php

namespace App\Domains\TaskHandler;

abstract class TaskHandler extends \Phalcon\Mvc\User\Component{
    
    abstract public function recieveDataFromAPI(string $url);
    
    abstract public function returnData(int $resource = 0, $sub_resource = 0);
    
    abstract public function shiftTask($data);
    
    abstract public function updateTask($data);
}