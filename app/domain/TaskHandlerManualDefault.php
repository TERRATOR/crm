<?php

namespace App\Domains\TaskHandler;

use App\Models\{Tasks,TypesOfTasks};
use App\Domains\TaskHandler\TaskHandler;

class TaskHandlerManualDefault extends TaskHandler {

    use \App\Traits\Singleton; // trait

    const UPDATE_TASK = 1;
    const DELEGATE_TASK = 2;
    const CHANGE_TASK_STATUS = 3;

    /**
     * Returns data recieved from API
     * @return Array
     */
    public function returnData(int $resource = 0, $sub_resource = 0) {
        $resource_name = "";
        if ($resource != 0) {
            $resource_name = Tasks::returnResourceTypes($resource);
            if (empty($resource_name)) {
                throw new \Exception('Ресурс по данному идентификатору отсутствует');
            }
        }
        $sub_resource_name = "";
        if ($sub_resource != 0) {
            $sub_resource_name = Tasks::returnResourceTypes($sub_resource);
            if (empty($sub_resource_name)) {
                throw new \Exception('Подресурс по данному идентификатору отсутствует');
            }
        }

        //$resourceData = $this->recieveDataFromAPI($resource_name . "?queryID=" . $resource);
        if (!empty($this->sub_resource_name)) {
            //$subResourceData = $this->recieveDataFromAPI($sub_resource_name . "?queryID=" . $sub_resource);
        }
        $some_fields = [];
        return ['resource' => $resourceData, 'subresourceData' => $subResourceData, 'fields' => $some_fields];
    }
    
    /**
     * Shifts task (Creates new task and close old one)
     * @param type $data
     * @throws \Exception
     */
    public function shiftTask($data) {
        $currentTask = Tasks::findFirstById($data['id']);
        if($currentTask == false) {
            throw new \Exception('Задача по данному идентификатору отсутствует.');
        }
        
        // create new child task
        $newTask = new Tasks();
        $newTask->attributes = $currentTask->attributes;
        $newTask->assign($data);
        $newTask->clone_parent_id = $data['id'];
        if($newTask->create() == false) {
            $messages = "";
            foreach($newTask->getMessages() as $message) {
                $messages .= $message."<br>";
            }
            throw new \Exception($messages);
        }
        
        $currentTask->clone_child_id = $newTask->id;
        $currentTask->status = Tasks::STATUS_CLONE_CLOSE;
        $currentTask->comment = $data['comment'];
        if($currentTask->update() == false) {
            $messages = "";
            foreach($currentTask->getMessages() as $message) {
                $messages .= $message."<br>";
            }
            throw new \Exception($messages);
        }
    }

    /**
     * Updates task
     * @param type $data
     * @throws \Exception
     */
    public function updateTask($data) {
        $currentTask = Tasks::findFirstById($data['id']);
        if($currentTask == false) {
            throw new \Exception('Задача по данному идентификатору отсутствует.');
        }
        $currentTask->assign($data);
        if($currentTask->update() == false) {
            $messages = "";
            foreach($currentTask->getMessages() as $message) {
                $messages .= $message."<br>";
            }
            throw new \Exception($messages);
        }
    }
    
    
}
