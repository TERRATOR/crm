<?php

namespace App\Controllers;

use App\Domains\TaskHandler\{
    TaskHandlerAdvertising,
    TaskHandlerHoliday,
    TaskHandlerManualDefault,
    TaskHandlerObjOperationStatus
};
use App\Models\{
    Tasks,
    TypesOfTasks,
    Users,
    Cards
};
use App\Controllers\ControllerBase;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\{
    PresenceOf,
    Email,
    Identical,
    StringLength,
    Between,
    Numericality
};

use \App\Validations\ValidationClass;

class IndexController extends ControllerBase {
    
    use \App\Traits\Singleton;

    public function initialize() {
        
    }

    public function indexAction() {
        if ($this->request->isPost()) {
            $rules = [['post_identity', 'strlen', 'max' => 32, 'messageMaximum' => 'String is too big.'],
                      ['email', ['email', 'strlen'], 'min' => 8, 'max' => 64, 'messageMaximum' => 'Email is too big.', 'messageMinimum' => 'Email is too short'],
                      ['number', 'num', 'message' => 'Variable is not a number.'],
                      ['ip', 'ip', 'message' => 'IP address is incorrect.']];
            
            $isomorphValidation = new ValidationClass($rules);
            $validate = $isomorphValidation->validate($this->request->getPost());
            if(count($validate) > 0) {
                return $this->response->setJsonContent(['code' => 666, 'data' => $validate]);
            } else {
                return $this->response->setJsonContent(['code' => 200, 'data' => 'Validation was successfully passed. Congratulations.', 
                                                        'data' => $this->request->getPost()]);
            }
            
        }
        $tasks = Tasks::query()->getActiveRecords(1)->execute();
        $this->view->tasks = $tasks;
    }
    
    /**
     * Returns data of task
     * @return type
     */
    public function getDataForTaskAction() {
        $task = Tasks::findFirstById($this->request->getQuery('id', 'int'));
        if ($task == null) {
            return $this->response->setJsonContent(['code' => 404, 'status' => 'Задача по данному идентификатору отсутствует в базе данных.']);
        }

        $handlerClass = Tasks::returnHandlerClass($task->handler);
        if ($handlerClass === false) {
            return $this->response->setJsonContent(['code' => 787, 'status' => 'Не удалось найти обработчик по данному идентификатору.']);
        }

        $handler = new $handlerClass();
        $resource_data = $handler->returnData($task->resource, $task->sub_resource);
        try {
            $sanitized_data = $handler->sanitizeData($this->request->getQuery(), '\App\Models\Tasks');
        } catch (\Exception $ex) {
            return $this->response->setJsonContent(['code' => 777, 'status' => $ex->getMessage(),
                        'sdsd' => str_replace("\/", "\\", '\App\Models\Tasks')]);
        }
        return $this->response->setJsonContent(['code' => 200, 'resource_data' => $resource_data, 'task' => $task, 'filter' => Tasks::FILTER_METHODS,
                'sanitized_data' => $sanitized_data,
                '$this->request->getQuery()' => $this->request->getQuery()]);
    }

    /**
     * Creates new task
     * @return type
     */
    public function createTaskAction() {
        $new_task = new Tasks();
        $new_task->assign($this->request->getPost());
        $errors = $this->validateModel($new_task);
        if($errors !== true) {
            return $this->response->setJsonContent(['code' => 380, 'status' => $errors,
                        'debug' => [$this->request->getPost(), $this->request->getQuery()]]);
        }
        if ($new_task->create() == false) {
            $messages = "";
            foreach($new_task->getMessages() as $message) {
                $messages .= $message."<br>";
            }
            return $this->response->setJsonContent(['code' => 141, 'status' => $messages]);
        }
        return $this->response->setJsonContent(['code' => 200, 'status' => 'Задача успешно создана']);
    }

    /**
     * Changes task status
     * @return type
     */
    public function changeStatusAction() {
        // It should be (updateTask | shiftTask)
        $type = $this->request->getPost("type", 'string');
        try {
            $handler = new TaskHandlerManualDefault();
            $handler->$type($this->request->getPost());
        } catch (\Exception $ex) {
            return $this->response->setJsonContent(['code' => 658, 'status' => $ex->getMessage()]);
        }
        return $this->response->setJsonContent(['code' => 200, 'status' => 'Статус задачи успешно изменён']);
    }

    /**
     * Deletes task (soft delete)
     * @return type
     */
    public function deleteTaskAction() {
        $task = Tasks::findFirstById($this->request->getPost("id", 'int'));
        if($task == false) {
            return $this->response->setJsonContent(['code' => 658, 'status' => 'Задача отсутствует по данному идентификатору']);
        }
        if($task->delete() == false) {
            $messages = "";
            foreach($new_task->getMessages() as $message) {
                $messages .= $message."<br>";
            }
            return $this->response->setJsonContent(['code' => 758, 'status' => $messages]);
        }
        return $this->response->setJsonContent(['code' => 200, 'status' => 'Задача успешно удалена']);
    }
}
