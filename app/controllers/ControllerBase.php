<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ControllerBase extends Controller
{

    /**
     * Отсылает почту через G Suite GMail
     * @param type $email
     * @param type $subject
     * @param type $body
     * @return boolean
     */
    public function sendEmail($email, $subject, $body, $attachments = []) {
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            $mail->CharSet = 'UTF-8';
            $mail->setFrom('admin@dts-auto.com', 'DTS Auto');
            $mail->addAddress($email);     // Add a recipient
            //Attachments
            if (count($attachments) > 0) {
                foreach ($attachments as $attachment) {
                    $mail->addAttachment($attachment);
                    #$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                }
            }

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = mb_convert_encoding($subject, "UTF-8", "auto");
            $mail->Body = $body;
            $mail->AltBody = strip_tags($body);

            $mail->send();
            return true;
        } catch (\Exception $e) {
            return $mail->ErrorInfo;
        }
    }
    
}
